<?php
session_start();
if(empty($_SESSION['Inicio'])){
  header('Location:login.php');
}


?>

<!doctype html>
<html lang="es">
  <head>
    <!-- Required meta tags -->
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1">

    <!-- Bootstrap CSS -->
    <link href="https://cdn.jsdelivr.net/npm/bootstrap@5.1.3/dist/css/bootstrap.min.css" rel="stylesheet" integrity="sha384-1BmE4kWBq78iYhFldvKuhfTAU6auU8tT94WrHftjDbrCEXSU1oBoqyl2QvZ6jIW3" crossorigin="anonymous">

    <title>Registra Alumno</title>

    
    <style>
      main{
        margin:10px;
      }

    </style>

  </head>
  <body>
  <nav class="navbar navbar-expand-lg navbar-light bg-light">
  <div class="container-fluid">
    <a class="navbar-brand" href="info.php">Home</a>
    <button class="navbar-toggler" type="button" data-bs-toggle="collapse" data-bs-target="#navbarNav" aria-controls="navbarNav" aria-expanded="false" aria-label="Toggle navigation">
      <span class="navbar-toggler-icon"></span>
    </button>
    <div class="collapse navbar-collapse" id="navbarNav">
      <ul class="navbar-nav">
        <li class="nav-item">
          <a class="nav-link" href="formulario.php">Registrar Alumno</a>
        <li class="nav-item">
          <a class="nav-link" href="logout.php">Cerrar Sesión</a>
        </li>
      </ul>
    </div>
  </div>
</nav>




<main>
    <form method="POST" action="procesarInfo.php" class="row g-3 needs-validation" novalidate>
    <div class="col-md-4">
        <label for="validationCustom01" class="form-label">Número de cuenta</label>
        <input type="number" class="form-control" name="num_cta"  required>
    </div>
    <div class="col-md-4">
        <label for="validationCustom02" class="form-label">Nombre</label>
        <input type="text" class="form-control" name="nombre"  required>
    </div>
    
    <div class="col-md-4">
        <label for="validationCustom02" class="form-label">Primer Apellido</label>
        <input type="text" class="form-control" name="primer_apellido"  required>
    </div>
    
    <div class="col-md-4">
        <label for="validationCustomUsername" class="form-label">Segundo Apellido</label>
        <div class="input-group has-validation">
        <input type="text" class="form-control" name="segundo_apellido"  required>
        </div>
    </div>



    <div class="col-md-3">
        <label class="form-label">Género</label>
                    <label class="form-radio">
                        <input type="radio" name="genero" value="H" checked>
                        <i class="form-icon"></i> Hombre
                    </label>
                    <label class="form-radio">
                        <input type="radio" name="genero" value="M">
                        <i class="form-icon"></i> Mujer
                    </label>
                    <label class="form-radio">
                        <input type="radio" name="genero" value="O">
                        <i class="form-icon"></i> Otro
                    </label>
    </div>

    <div class="col-md-3">
    <label class="form-label" for="input-date">Fecha de Nacimiento</label>
        <input name="fecha_nac" class="form-input " type="date" id="input-date" placeholder="fecha_nac">
        </div>
            

    <div class="col-md-3">
        <label for="validationCustom05" class="form-label">Contraseña</label>
        <input type="password" class="form-control" id="validationCustom05" name="contrasena" required>
    </div>



    
    <div class="col-12">
        <button class="btn btn-primary" type="submit">Aceptar</button>
    </div>
    </form>




</main>



    <!-- Optional JavaScript; choose one of the two! -->

    <!-- Option 1: Bootstrap Bundle with Popper -->
    <script src="https://cdn.jsdelivr.net/npm/bootstrap@5.1.3/dist/js/bootstrap.bundle.min.js" integrity="sha384-ka7Sk0Gln4gmtz2MlQnikT1wXgYsOg+OMhuP+IlRH9sENBO0LRn5q+8nbTov4+1p" crossorigin="anonymous"></script>

    <!-- Option 2: Separate Popper and Bootstrap JS -->
    <!--
    <script src="https://cdn.jsdelivr.net/npm/@popperjs/core@2.10.2/dist/umd/popper.min.js" integrity="sha384-7+zCNj/IqJ95wo16oMtfsKbZ9ccEh31eOz1HGyDuCQ6wgnyJNSYdrPa03rtR1zdB" crossorigin="anonymous"></script>
    <script src="https://cdn.jsdelivr.net/npm/bootstrap@5.1.3/dist/js/bootstrap.min.js" integrity="sha384-QJHtvGhmr9XOIpI6YVutG+2QOK9T+ZnN4kzFN1RtK3zEFEIsxhlmWl5/YESvpZ13" crossorigin="anonymous"></script>
    -->
  </body>
</html>