<?php
//Muñoz Garcia Arturo
//Realizar una expresión regular que detecte emails correctos.
$email='juanito123@hotmai.com';
$resultado=preg_match('/^[A-z0-9\\._-]+@[A-z0-9][A-z0-9]*.([A-z0-9]*)/',$email);
if ($resultado){
    echo "Es un correo";
    echo '</br>';
}
else{
    echo "No es un correo";
    echo '</br>';
}


//Realizar una expresion regular que detecte Curps Correctos
$CURP='ABCD123456EFGHIJ78';
$resultado=preg_match('/[A-Z][A-Z][A-Z][A-Z][0-9][0-9][0-9][0-9][0-9][0-9][A-Z][A-Z][A-Z][A-Z][A-Z][A-Z][0-9][0-9]/',$CURP);
if ($resultado){
    echo "Es un CURP";
    echo '</br>';
}
else{
    echo "No es un CURP";
    echo '</br>';
}


//Realizar una expresion regular que detecte palabras de longitud mayor a 50
//formadas solo por letras.
$Palabra='AAAAAAApoAAAAAAAAAAAAAAAAAAppAAAAAAAAAAAAAAAAAAAzrAAAAAAAA';
$resultado=preg_match('/[A-Za-z]{50}/',$Palabra);
if ($resultado){
    echo "Es una palabra de mas de 50";
    echo '</br>';
}
else{
    echo "No es una palabra de mas de 50";
    echo '</br>';
}


//Crea una funcion para escapar los simbolos especiales.
$cadena='#Hola';
$resultado=preg_quote('*',$cadena);
echo $resultado;
echo '</br>';



//Crear una expresion regular para detectar números decimales.
$Decimal='-10.0';
$resultado=preg_match('/(-)?[0-9]+.[0-9]+/',$Decimal);
if ($resultado){
    echo "Es un úmero decimal";
    echo '</br>';
}
else{
    echo "No es un número Decimal";
    echo '</br>';
}

?>